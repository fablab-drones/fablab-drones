// Parametric flying wing using the s3021 airfoil
// (CC) 2012 David Pello, FabLAB Asturias
// Creative commons CC-BY-SA http://creativecommons.org/licenses/by-sa/3.0/es/
// All measurements are in millimeters except propeller size (inches)

wingspan = 1000;
retract = 200;
airfoil_big_height = 30;
airfoil_small_height = 20;
dihedral_height = 0;
spar_lenght = 600;
spar_radius = 6;
spar_position_from_front = 130;
propeller_inches = 8;
propeller_slot_pos = 250;

// If you want to change the airfoil, you have to provide a dxf file with the airfoil
// with the front base at (0,0) see original, and the initial height of the airfoil in 
// the dxf in mm
airfoil = "s3021-il-30mm.dxf";
airfoil_height = 30;

module spar() {
	hull() {
		rotate([90,0,0])
			cylinder(r=spar_radius/2, h=spar_lenght, center=true);
		translate([0,0,dihedral_height/2+5])
			rotate([90,0,0])
				cylinder(r=spar_radius/2, h=spar_lenght, center=true);
	}
}

module s3021_small() {
	scale(airfoil_small_height/airfoil_height)
		rotate([90,0,0])
			dxf_linear_extrude(file=airfoil, layer = "0", height=0.1, convexity=10, center=true);
}

module s3021_big() {
	scale(airfoil_big_height/airfoil_height)
		rotate([90,0,0])
			dxf_linear_extrude(file=airfoil, layer = "0", height=0.1, convexity=10, center=true);
}


module right_wing() {
	hull() {
		s3021_big();
		translate([retract,wingspan/2,dihedral_height])
			s3021_small();
	}
}

module left_wing() {
	hull() {
		s3021_big();
		translate([retract,-wingspan/2,dihedral_height])
			s3021_small();
	}
}

module propeller_slot() {
	cube([50,(propeller_inches*25.4)+20, 30], center=true);
}



module wing() {
	difference() {
		union() {
			right_wing();
			left_wing();
		}
		translate([spar_position_from_front,0,0])
			spar();
		translate([propeller_slot_pos, 0, airfoil_big_height/2])
			propeller_slot();
	}
}

wing();
